FROM nginx:alpine
COPY default.conf /etc/nginx/conf.d/default.conf
COPY  static-website-example/ /usr/share/nginx/html/
EXPOSE 80
EXPOSE 443